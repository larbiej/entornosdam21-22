package main;

import java.util.Scanner;

public class Main {
	static Scanner in = new Scanner(System.in);
	public static void main(String[] args) {
		/*Realizar un programa que calcule el salario de los trabajadores de una empresa en funci�n del n�mero de horas
trabajadas y la categor�a profesional
Los empleados de 1� categor�a cobrar�n 40� la hora normal y 50� la hora extra y al sueldo bruto se le aplicar�
una retenci�n del 16%
Los empleados de 2� categor�a cobrar�n 30� la hora normal y 40� la hora extra y al sueldo bruto se le aplicar�
una retenci�n del 14%
Los empleados de 3� categor�a cobrar�n 20� la hora normal y 30� la hora extra y al sueldo bruto se le aplicar�
una retenci�n del 12%
Visualizar el Sueldo Bruto y el sueldo Neto para cada uno
de los empleados. El proceso finalizar� cuando el usuario
pulse * en el campo nombre o bien est�n introducidos los
datos de todos los empleados.
Utilizar un m�todo que calcule el sueldo bruto y otra que
calcule el sueldo neto
Nombre (�*� para salir): xxx
Horas trabajadas : xxx
Categor�a Profesional : xxx
N�mero de horas extras: xxx
Sueldo Bruto xxx
Sueldo Neto xxx

*/
		String nombre;
		int horas=0;
		char cat='*';
		int hext=0;
		float sueldoB=0;
		float sueldoN;
		do {	
			System.out.println("***C�lculo salario***");
			System.out.println("Nombre: ('*' para salir)");
			nombre=in.nextLine();
			if(nombre.charAt(0)!='*') {
				System.out.println("Horas trabajadas");
				horas=in.nextInt();
				in.nextLine();
				System.out.println("Cagetor�a profesional: \n1� categor�a (A) \n2� categor�a (B) \n3� categor�a (C)");
				cat=in.nextLine().charAt(0);
				cat=Character.toLowerCase(cat);
				System.out.println("Horas extra:");
				hext=in.nextInt();
				in.nextLine();
			
			sueldoB=sueldoB(cat, horas, hext);
			System.out.println("El sueldo bruto es " + sueldoB);
			sueldoN=sueldoN(sueldoB, cat);
			System.out.println("El sueldo neto es " + sueldoN);
			}
		}while(nombre.charAt(0)!='*');	
		
		
		
	
		System.out.println("Fin del programa");
		
	}
	
	static float sueldoB(char cat, int horas, int hext) {
		float sueldoB=0;
		switch(cat) {
			case 'a': sueldoB=horas*40+hext*50;
					break;
			case 'b': sueldoB=horas*30+hext*40;
					break;
			case 'c': sueldoB=horas*20+hext*30;
					break;
		}
		return sueldoB;
	}
	
	static float sueldoN(float sueldoB, char cat) {
		float sueldoN=0;
		switch(cat) {
			case 'a': sueldoN=sueldoB-sueldoB*16/100;
					break;
			case 'b': sueldoN=sueldoB-sueldoB*14/100;
					break;
			case 'c': sueldoN=sueldoB-sueldoB*12/100;
					break;
		}
		return sueldoN;
	}
	
}
